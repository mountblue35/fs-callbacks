const path = require("path")
const fs = require("fs")


function problem1(files) {
    const dir = path.join(__dirname, './random')

    fs.mkdir(dir, { recursive: true }, (err) => {
        if (err) {
            console.log("Error:", err)
        } else {
            console.log("Directory has been made")

            for (let index = 0; index < files; index++) {

                let list = Array.from({
                    length: 10
                }, () => Math.floor(Math.random() * 10));

                let fileName = `random${index + 1}`
                fs.writeFile(path.join(__dirname, `./random/${fileName}`),
                    JSON.stringify(list),
                    "utf8",
                    function (err) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log(`File ${fileName} has been written`)


                            fs.unlink(path.join(dir, fileName), (err) => {
                                if (err) {
                                    console.log(err)
                                } else {
                                    console.log(`File ${fileName} has been deleted`)
                                }
                            })
                        }
                    })
            }
        }
    })

}

module.exports = problem1