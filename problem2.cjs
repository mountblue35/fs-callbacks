const path = require("path")
const fs = require("fs")

function problem2() {
    let dir = __dirname


    // 1. Read the given file lipsum.txt
    fs.readFile(path.join(dir, './lipsum.txt'), "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        } else {

            // 2. Convert the content to uppercase & write to a new file

            let upperData = data.replaceAll("\n\n", "").toUpperCase()
            fs.writeFile(path.join(dir, 'lipsumUpper.txt'),
                JSON.stringify(upperData),
                "utf-8",
                function (err) {
                    if (err) {
                        console.log(err)
                    } else {
                        fs.appendFile(path.join(dir, 'filenames.txt'),
                            ('lipsumUpper.txt' + '\n'),
                            "utf-8",
                            function (err) {
                                if (err) {
                                    console.log(err)
                                }
                                else {

                                    // 3. Read the new file and convert it to lower case. Then split the contents into sentences.        
                                    fs.readFile(path.join(dir, 'lipsumUpper.txt'),
                                        "utf-8",
                                        (err, data1) => {
                                            if (err) {
                                                console.log(err)
                                            } else {
                                                // Main Logic
                                                let lowerData = data1
                                                    .slice(1, data1.length - 3)
                                                    .toLowerCase()
                                                lowerData = lowerData.split(". ")

                                                fs.writeFile(path.join(dir, 'lipsumLower.txt'),
                                                    JSON.stringify(lowerData),
                                                    "utf-8",
                                                    function (err) {
                                                        if (err) {
                                                            console.log(err)
                                                        }
                                                    }
                                                )
                                                fs.appendFile(path.join(dir, 'filenames.txt'),
                                                    ('lipsumLower.txt' + '\n'),
                                                    "utf-8",
                                                    function (err) {
                                                        if (err) {
                                                            console.log(err)
                                                        } else {
                                                            fs.readFile(path.join(dir, './lipsumLower.txt'),
                                                                "utf-8",
                                                                (err, data2) => {
                                                                    if (err) {
                                                                        console.log(err)
                                                                    } else {
                                                                        let cleanData2 = data2
                                                                            .slice(2, data2.length - 2)
                                                                            .split("\",\"")

                                                                        let sortedData2 = cleanData2.sort()

                                                                        fs.writeFile(path.join(dir, 'lipsumSorted.txt'),
                                                                            JSON.stringify(sortedData2),
                                                                            "utf-8",
                                                                            function (err) {
                                                                                if (err) {
                                                                                    console.log(err)
                                                                                } else {
                                                                                    fs.appendFile(path.join(dir, 'filenames.txt'),
                                                                                        ('lipsumSorted.txt' + '\n'),
                                                                                        "utf-8",
                                                                                        function (err) {
                                                                                            if (err) {
                                                                                                console.log(err)
                                                                                            } else {
                                                                                                fs.readFile(path.join(dir, './filenames.txt'),
                                                                                                    "utf-8",
                                                                                                    (err, data3) => {
                                                                                                        if (err) {
                                                                                                            console.log(err)
                                                                                                        } else {
                                                                                                            let arrayOfFiles = data3
                                                                                                                .split("\n")
                                                                                                            arrayOfFiles = arrayOfFiles.slice(0, arrayOfFiles.length - 1)

                                                                                                            arrayOfFiles.forEach(file => {
                                                                                                                fs.unlink(path.join(dir, file), err => {
                                                                                                                    if (err) {
                                                                                                                        console.log(err)
                                                                                                                    }
                                                                                                                })
                                                                                                            })
                                                                                                        }
                                                                                                    }
                                                                                                )
                                                                                            }
                                                                                        }
                                                                                    )

                                                                                }
                                                                            }
                                                                        )
                                                                    }
                                                                }
                                                            )

                                                        }
                                                    })
                                            }
                                        })
                                }
                            })

                    }
                }
            )
        }












    })

}

module.exports = problem2